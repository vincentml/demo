<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    
    <sch:pattern>
        <sch:rule context="article-meta">
            <sch:assert id="am-0001"  role="error" test="count(article-id[@pub-id-type='doi']) = 1">An article should have one DOI tagged in &lt;article-id> with pub-id-type="doi"</sch:assert>
            <sch:report id="am-0002"  role="warn" test="ancestor::article[@article-type='book-review'] and not(product)">A book review article should have details of the book(s) being reviewed tagged in &lt;product> element(s)</sch:report>
        </sch:rule>
    </sch:pattern>

</sch:schema>